_default:
    @just --list

pull-subtree:
    git subtree pull --prefix elgato-streamdeck git@github.com:MechWipf/elgato-streamdeck.git main --squash

push-subtree BRANCH="main":
    git subtree push --prefix elgato-streamdeck git@github.com:MechWipf/elgato-streamdeck.git {{ BRANCH }}

run:
    cargo build --release
    taskset -ac 6-11 cargo run --release

compile-windows:
    podman run --rm --name rust -v {{ justfile_directory() }}:/workspace:z -w /workspace rust sh -c "\
        set eux;\
        apt-get update;\
        apt-get install mingw-w64 -y;\
        rustup target add x86_64-pc-windows-gnu;\
        cargo build --target=x86_64-pc-windows-gnu --release"
