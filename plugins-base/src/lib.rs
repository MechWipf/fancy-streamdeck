use abi_stable::{
    export_root_module,
    prefix_type::PrefixTypeTrait,
    sabi_extern_fn,
    std_types::{RStr, RVec},
};
use plugins_interface::{FancyDeckPlugin, FancyDeckPluginRef};

mod shell;

#[export_root_module]
fn instantiate_root_module() -> FancyDeckPluginRef {
    FancyDeckPlugin { run }.leak_into_prefix()
}

#[sabi_extern_fn]
fn run(args: RVec<RStr>) {
    let args: Vec<&str> = args.iter().map(|x| x.as_str()).collect();
    if let Err(err) = shell::run(&args) {
        eprintln!("plugins-base: {}", err);
    }
}
