use anyhow::bail;
use std::process::Command;

#[no_mangle]
pub fn run(args: &[&str]) -> anyhow::Result<()> {
    let mut cmd = Command::new("/usr/bin/bash");
    cmd.arg("-c");
    cmd.arg(args[0]);
    let mut child = cmd.spawn().or_else(|x| bail!(""))?;
    let _ = child.wait()?;

    Ok(())
}
