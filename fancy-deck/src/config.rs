use std::{path::Path, sync::Arc};

use abi_stable::library::RootModule;
use bevy::{
    ecs::system::Commands,
    hierarchy::BuildChildren,
    render::color::Color,
    ui::{node_bundles::NodeBundle, JustifyContent, Style, UiRect, Val},
    utils::default,
    utils::hashbrown::HashMap,
};
use plugins_interface::FancyDeckPluginRef;

use crate::{Bindings, Hotkey, MyGlyph};

pub fn config(commands: &mut Commands) {
    // let cube_colors = [
    //     Color::rgb_u8(255, 255, 255),
    //     Color::rgb_u8(209, 48, 255),
    //     Color::rgb_u8(123, 140, 25),
    //     Color::rgb_u8(119, 67, 255),
    //     Color::rgb_u8(119, 67, 25),
    //     Color::rgb_u8(34, 140, 25),
    //     Color::rgb_u8(37, 64, 114),
    //     Color::rgb_u8(216, 199, 114),
    // ];

    let mut bindings = Bindings {
        event: HashMap::new(),
        update: HashMap::new(),
    };

    conf_fix_monitor(commands, &mut bindings);
    conf_fix_sound(commands, &mut bindings);

    commands.insert_resource(bindings);
}

fn conf_fix_monitor(commands: &mut Commands, bindings: &mut Bindings) {
    let id = "fix_monitor";

    commands.spawn((
        MyGlyph::Cube(Color::rgb_u8(255, 255, 255)),
        Hotkey {
            id,
            x: 0,
            y: 0,
            init: false,
            _state: false,
        },
    ));
    bindings.event.insert(
        id,
        Arc::new(|_, event, _| {
            if let crate::plugins::streamdeck::StreamDeckEvent::ButtonPress(_) = event {
                let plugin =
                    FancyDeckPluginRef::load_from_file(Path::new("libplugins_base.so")).unwrap();
                let fn_run = plugin.run().unwrap();

                fn_run(vec!["just --justfile ~/.user.justfile fix-monitor".into()].into());
            }

            Ok(())
        }),
    );
    bindings.update.insert(id, Arc::new(|| Ok(())));
}

fn conf_fix_sound(commands: &mut Commands, bindings: &mut Bindings) {
    let id = "fix_sound";

    commands.spawn((
        MyGlyph::Cube(Color::rgb_u8(209, 48, 255)),
        Hotkey {
            id,
            x: 1,
            y: 0,
            init: false,
            _state: false,
        },
    ));
    bindings.event.insert(
        id,
        Arc::new(|_, event, _| {
            if let crate::plugins::streamdeck::StreamDeckEvent::ButtonPress(_) = event {
                let plugin =
                    FancyDeckPluginRef::load_from_file(Path::new("libplugins_base.so")).unwrap();
                let fn_run = plugin.run().unwrap();

                fn_run(vec!["just --justfile ~/.user.justfile fix-sound".into()].into());
            }

            Ok(())
        }),
    );
    bindings.update.insert(id, Arc::new(|| Ok(())));
}
