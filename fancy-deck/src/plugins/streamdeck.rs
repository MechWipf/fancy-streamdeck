use std::time::Duration;

use anyhow::Context;
use bevy::app::App;
use bevy::prelude::*;
use bevy::utils::synccell::SyncCell;
use elgato_streamdeck::images::ImageRect;
use elgato_streamdeck::info::Kind;
use elgato_streamdeck::{new_hidapi, StreamDeck};

pub struct StreamDeckPlugin;

impl Plugin for StreamDeckPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Time::<Fixed>::from_seconds(1.0 / 60.0))
            .insert_resource(StreamDeckInput {
                button_state: vec![false; 8],
                encoder_state: vec![false; 4],
            })
            .add_event::<StreamDeckEvent>()
            .add_systems(FixedUpdate, listener.pipe(handle_error))
            .add_systems(PostUpdate, render);
    }
}

#[derive(Component)]
pub struct StreamDeckKey {
    pub index: u8,
    pub img: Option<Vec<u8>>,
}

#[derive(Component)]
pub struct StreamDeckLcd {
    x: u16,
    y: u16,
    pub img: Option<ImageRect>,
}

impl StreamDeckLcd {
    pub fn new(x: u16, y: u16, img: Option<ImageRect>) -> Self {
        Self { x, y, img }
    }
}

#[derive(Debug, Event)]
pub enum StreamDeckEvent {
    ButtonPress(u8),
    ButtonRelease(u8),
    EncoderPress(u8),
    EncoderRelease(u8),
    EncoderTwist(u8, i8),

    Disconnected,
    Connected(Kind, String, String),
}

#[derive(Resource)]
pub struct StreamDeckInput {
    button_state: Vec<bool>,
    encoder_state: Vec<bool>,
}

#[derive(Resource)]
struct PluginRes {
    deck: SyncCell<StreamDeck>,
}

fn listener(
    mut commands: Commands,
    streamdeck: Option<ResMut<PluginRes>>,
    mut streamdeck_input: ResMut<StreamDeckInput>,
    mut events: ResMut<Events<StreamDeckEvent>>,
) -> anyhow::Result<()> {
    if let Some(mut res_deck) = streamdeck {
        let input = {
            let deck = res_deck.deck.get();
            deck.read_input(Some(Duration::from_millis(1)))
        };

        if let Ok(input) = input {
            match input {
                elgato_streamdeck::StreamDeckInput::NoData => (),
                elgato_streamdeck::StreamDeckInput::ButtonStateChange(state) => {
                    for (i, s) in state.iter().enumerate() {
                        let last = streamdeck_input.button_state.get(i).unwrap_or(&false);

                        if *s != *last {
                            if *s {
                                events.send(StreamDeckEvent::ButtonPress(i as u8))
                            } else {
                                events.send(StreamDeckEvent::ButtonRelease(i as u8))
                            }
                        }
                    }

                    streamdeck_input.button_state = state;
                }
                elgato_streamdeck::StreamDeckInput::EncoderStateChange(state) => {
                    for (i, s) in state.iter().enumerate() {
                        let last = streamdeck_input.encoder_state.get(i).unwrap_or(&false);

                        if *s != *last {
                            if *s {
                                events.send(StreamDeckEvent::EncoderPress(i as u8))
                            } else {
                                events.send(StreamDeckEvent::EncoderRelease(i as u8))
                            }
                        }
                    }

                    streamdeck_input.encoder_state = state;
                }
                elgato_streamdeck::StreamDeckInput::EncoderTwist(state) => {
                    for (i, s) in state.iter().enumerate() {
                        if *s != 0 {
                            events.send(StreamDeckEvent::EncoderTwist(i as u8, *s));
                        }
                    }
                }
                elgato_streamdeck::StreamDeckInput::TouchScreenPress(x, y) => {
                    println!("TouchScreenPress {:?}", (x, y))
                }
                elgato_streamdeck::StreamDeckInput::TouchScreenLongPress(x, y) => {
                    println!("TouchScreenLongPress {:?}", (x, y))
                }
                elgato_streamdeck::StreamDeckInput::TouchScreenSwipe(start, end) => {
                    println!("TouchScreenSwipe {:?}", (start, end))
                }
            }
        } else {
            events.send(StreamDeckEvent::Disconnected);
            commands.remove_resource::<PluginRes>();
        }
    } else {
        let hid = new_hidapi().context("Can't connect to HID.")?;
        let devices = elgato_streamdeck::list_devices(&hid);
        if !devices.is_empty() {
            let (kind, serial) = elgato_streamdeck::list_devices(&hid).remove(0);

            let device =
                StreamDeck::connect(&hid, kind, &serial).context("Failed to connect to deck.")?;

            let serial_number = device
                .serial_number()
                .context("Unable to fetch serial number.")?;
            let firmware_version = device
                .firmware_version()
                .context("Unable to fetch firmware version.")?;

            device.reset()?;
            device.set_brightness(30)?;

            events.send(StreamDeckEvent::Connected(
                device.kind(),
                serial_number,
                firmware_version,
            ));

            commands.insert_resource(PluginRes {
                deck: SyncCell::new(device),
            });

            for btn_state in &mut streamdeck_input.button_state {
                *btn_state = false;
            }

            for enc_state in &mut streamdeck_input.encoder_state {
                *enc_state = false;
            }
        }
    }

    Ok(())
}

fn render(
    query_keys: Query<&StreamDeckKey, Changed<StreamDeckKey>>,
    query_lcd: Query<&StreamDeckLcd, Changed<StreamDeckLcd>>,
    streamdeck: Option<ResMut<PluginRes>>,
) {
    if let Some(mut res_deck) = streamdeck {
        let deck = res_deck.deck.get();

        for key in query_keys.iter() {
            if let Some(img) = &key.img {
                let _ = deck.write_image(key.index, img);
            }
        }

        for lcd in query_lcd.iter() {
            if let Some(img) = &lcd.img {
                let _ = deck.write_lcd(lcd.x, lcd.y, img);
            }
        }
    }
}

fn handle_error(In(result): In<anyhow::Result<()>>) {
    if let Err(e) = result {
        eprintln!("Error occurred: {}", e);
    }
}
