mod config;
mod plugins;

use std::sync::Arc;
use std::time::Duration;

use bevy::app::ScheduleRunnerPlugin;
use bevy::core_pipeline::clear_color::ClearColorConfig;

use bevy::pbr::ClusterConfig;
use bevy::prelude::*;
use bevy::render::camera::RenderTarget;
use bevy::render::renderer::RenderDevice;
use bevy::render::view::RenderLayers;
use bevy::utils::hashbrown::HashMap;
use bevy::{
    app::{App, Startup},
    asset::Assets,
    ecs::system::{Commands, ResMut},
    prelude::default,
    render::{
        mesh::{shape, Mesh},
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        texture::Image,
    },
};
use elgato_streamdeck::images::{convert_image, ImageRect};
use image::imageops::FilterType;
use plugins::image_copy::{ImageCopier, ImageCopyPlugin};
use plugins::streamdeck::{StreamDeckEvent, StreamDeckKey, StreamDeckLcd, StreamDeckPlugin};

const HEIGHT: u32 = 1024;
const WIDTH: u32 = 1024;

pub fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            StreamDeckPlugin,
            ScheduleRunnerPlugin {
                run_mode: bevy::app::RunMode::Loop {
                    wait: Duration::from_secs_f32(1.0 / 15.0).into(),
                },
            },
            ImageCopyPlugin,
        ))
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (
                handle_connect.pipe(handle_error),
                update,
                handle_sd_input,
                rotator_system,
                init_hotkeys,
                fix_render_layer,
            ),
        )
        .run();
}

#[derive(Component)]
enum MyGlyph {
    Cube(Color),
    _Object,
}

#[derive(Component)]
struct MyBackgroundPlane;

#[derive(Component)]
struct MyCamera;

#[derive(Component, Deref, DerefMut)]
struct ImageToSave(Handle<Image>);

#[derive(Resource, Debug)]
struct UiNodes(Vec<Entity>);

fn setup_rendertarget(
    commands: &mut Commands,
    images: &mut ResMut<Assets<Image>>,
    render_device: &Res<RenderDevice>,
) -> RenderTarget {
    let size = Extent3d {
        width: WIDTH,
        height: HEIGHT,
        ..default()
    };
    let mut render_image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsages::COPY_SRC
                | TextureUsages::COPY_DST
                | TextureUsages::TEXTURE_BINDING
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[],
        },
        ..default()
    };

    render_image.resize(size);
    let render_image_handle = images.add(render_image);

    let mut cpu_image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::COPY_DST | TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        },
        ..Default::default()
    };
    cpu_image.resize(size);
    let cpu_image_handler = images.add(cpu_image);

    commands.spawn(ImageCopier::new(
        render_image_handle.clone(),
        cpu_image_handler.clone(),
        size,
        render_device,
    ));

    commands.spawn(ImageToSave(cpu_image_handler));

    RenderTarget::Image(render_image_handle)
}

fn setup(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
    render_device: Res<RenderDevice>,
) {
    let render_layer = RenderLayers::layer(1);
    let render_target = setup_rendertarget(&mut commands, &mut images, &render_device);

    commands.insert_resource(AmbientLight {
        brightness: 1.0,
        color: Color::rgb_u8(255, 255, 255),
    });

    commands.spawn(Camera2dBundle::default());

    commands.spawn((
        MyCamera,
        Camera3dBundle {
            camera_3d: Camera3d {
                clear_color: ClearColorConfig::Custom(Color::BLACK),
                ..default()
            },
            camera: Camera {
                target: render_target,
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, 0.0))
                .looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
        render_layer,
        ClusterConfig::Single,
    ));

    let container = NodeBundle {
        style: Style {
            margin: UiRect {
                top: Val::Px(HEIGHT as f32 / 4.),
                bottom: Val::Px(HEIGHT as f32 / 4.),
                ..default()
            },
            width: Val::Px(HEIGHT as f32),
            height: Val::Px(HEIGHT as f32 / 2.),
            display: Display::Flex,
            flex_wrap: FlexWrap::Wrap,
            ..default()
        },
        ..default()
    };

    let parent = commands.spawn(container).id();
    let mut ui_nodes = vec![];

    for _i in 0..8 {
        let square = NodeBundle {
            style: Style {
                border: UiRect::all(Val::Px(2.)),
                width: Val::Px(HEIGHT as f32 / 4.),
                height: Val::Px(HEIGHT as f32 / 4.),
                justify_content: JustifyContent::Center,
                align_content: AlignContent::Center,
                ..default()
            },
            border_color: Color::rgb_u8(255, 0, 255).into(),
            ..default()
        };

        let child = commands.spawn(square).id();
        ui_nodes.push(child);

        commands.entity(parent).push_children(&[child]);
    }

    commands.insert_resource(UiNodes(ui_nodes));

    config::config(&mut commands);
}

fn init_hotkeys(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut q_hotkey: Query<(Entity, &mut Hotkey, &MyGlyph)>,
    q_camera: Query<(&Camera, &GlobalTransform, &RenderLayers), With<MyCamera>>,
    asset_server: Res<AssetServer>,
    ui_nodes: Res<UiNodes>,
) {
    let img_width = WIDTH as usize;
    let img_height = HEIGHT as usize;
    let step_x = img_width / 4;
    let step_y = img_height / 4;

    let (camera, camera_transform, render_layer) = q_camera.single();

    for (entity, mut hotkey, glyph) in &mut q_hotkey {
        if hotkey.init {
            continue;
        }

        hotkey.init = true;

        let x = hotkey.x;
        let y = hotkey.y;

        let cursor_position = Vec2::new(
            (step_x * x + step_x / 2) as f32,
            (img_height / 4) as f32 + (step_y * y + step_y / 2) as f32,
        );

        let Some(ray) = camera.viewport_to_world(camera_transform, cursor_position) else {
            println!("{}, {:?}", cursor_position, camera_transform);
            continue;
        };

        let global_cursor = ray.get_point(70.0);

        let obj = match glyph {
            MyGlyph::Cube(cube) => {
                let icon_plane_handle = meshes.add(Mesh::from(shape::Cube { size: 9.0 }));
                let plane_material_handle = materials.add(StandardMaterial {
                    base_color: *cube,
                    reflectance: 0.02,
                    unlit: false,
                    ..default()
                });

                PbrBundle {
                    mesh: icon_plane_handle,
                    material: plane_material_handle,
                    transform: Transform::from_translation(Vec3::new(
                        global_cursor.x,
                        global_cursor.y,
                        global_cursor.z,
                    ))
                    .looking_at(camera_transform.translation(), Vec3::X),
                    ..default()
                }
            }
            MyGlyph::_Object => unreachable!(),
        };

        commands.entity(entity).insert((obj, *render_layer));

        let text_node = TextBundle::from_section(
            "title",
            TextStyle {
                font: asset_server.load("fonts/Minecraft.ttf"),
                font_size: 40.,
                color: Color::WHITE,
            },
        );

        let text = commands.spawn((text_node, *render_layer)).id();

        commands
            .entity(ui_nodes.0[x + y * 4])
            .push_children(&[text]);
    }
}

fn fix_render_layer(
    mut _commands: Commands,
    q_render_layer: Query<&RenderLayers, (With<MyCamera>, Without<Node>)>,
    q_entity: Query<Entity, (With<Node>, Without<RenderLayers>)>,
) {
    let render_layer = q_render_layer.single();
    for entity in &q_entity {
        _commands.entity(entity).insert(*render_layer);
    }
}

fn rotator_system(time: Res<Time>, mut q_glyph: Query<&mut Transform, With<MyGlyph>>) {
    for mut transform in &mut q_glyph {
        transform.rotate_y(1.0 * time.delta_seconds());
    }
}

fn handle_sd_input(
    mut events: EventReader<StreamDeckEvent>,
    mut q_hotkey: Query<&mut Hotkey>,
    bindings: Res<Bindings>,
) {
    for event in events.read() {
        let hotkeys: Vec<_> = q_hotkey.iter().cloned().collect();
        let btn = match event {
            StreamDeckEvent::ButtonPress(btn) | StreamDeckEvent::ButtonRelease(btn) => {
                to_xy(*btn as u32, 4)
            }
            StreamDeckEvent::EncoderPress(btn)
            | StreamDeckEvent::EncoderRelease(btn)
            | StreamDeckEvent::EncoderTwist(btn, _) => to_xy(*btn as u32 + 8, 4),
            _ => (64, 64),
        };

        q_hotkey.for_each_mut(|mut hotkey| {
            if btn == (hotkey.x as u32, hotkey.y as u32) && bindings.event.contains_key(hotkey.id) {
                let res = bindings.event[hotkey.id](&mut hotkey, event, hotkeys.as_slice());
                handle_error(In(res));
            }
        });
    }
}

fn handle_connect(
    mut events: EventReader<StreamDeckEvent>,
    mut commands: Commands,
    q_key: Query<(Entity, &StreamDeckKey)>,
    q_lcd: Query<(Entity, &StreamDeckLcd)>,
) -> anyhow::Result<()> {
    for event in events.read() {
        match event {
            StreamDeckEvent::Disconnected => {
                println!("Stream Deck disconnected.");
                q_key.iter().for_each(|(x, _)| commands.entity(x).despawn());
                q_lcd.iter().for_each(|(x, _)| commands.entity(x).despawn());
            }
            StreamDeckEvent::Connected(kind, serial, firmware) => {
                println!(
                    "Connected to 'Stream Deck {}' ('{}') with version '{}'",
                    kind, serial, firmware
                );

                spawn_keys(&mut commands)?;
            }
            _ => (),
        }
    }

    Ok(())
}

fn spawn_keys(commands: &mut Commands<'_, '_>) -> Result<(), anyhow::Error> {
    for i in 0..8 {
        commands.spawn(StreamDeckKey {
            index: i,
            img: None,
        });
    }

    for i in 0..4 {
        commands.spawn(StreamDeckLcd::new(20 + i * 210, 0, None));
    }

    Ok(())
}

fn update(
    images_to_save: Query<&ImageToSave>,
    mut q_keys: Query<&mut StreamDeckKey>,
    mut q_lcds: Query<&mut StreamDeckLcd>,
    images: ResMut<Assets<Image>>,
) {
    for image in images_to_save.iter() {
        let img = images
            .get(&image.0)
            .cloned()
            .unwrap()
            .try_into_dynamic()
            .unwrap();

        let img_width = img.width();
        let img_height = img.height();
        let step_x = img_width / 4;
        let step_y = img_height / 4;

        for (idx, mut key) in (&mut q_keys).into_iter().enumerate() {
            let (x, y) = to_xy(idx as u32, 4);
            let crop = img.crop_imm(step_x * x, (img_width / 4) + step_y * y, step_x, step_y);
            key.img = convert_image(elgato_streamdeck::info::Kind::Plus, crop)
                .unwrap_or_default()
                .into();
        }

        let img = img.resize(100, 100, FilterType::CatmullRom);

        for mut lcd in &mut q_lcds {
            let data = ImageRect::from_image(img.clone()).unwrap();

            lcd.img = Some(data);
        }
    }
}

fn to_xy(index: u32, width: u32) -> (u32, u32) {
    (index - (index / width) * width, index / width)
}

fn handle_error(In(result): In<anyhow::Result<()>>) {
    if let Err(e) = result {
        eprintln!("Error occurred: {}", e);
    }
}

#[derive(Component, Clone, Debug)]
struct Hotkey {
    id: &'static str,
    init: bool,
    _state: bool,
    x: usize,
    y: usize,
}

#[derive(Resource)]
struct Bindings {
    event: HashMap<&'static str, Arc<EventCallback>>,
    update: HashMap<&'static str, Arc<UpdateCallback>>,
}

type EventCallback =
    dyn Fn(&mut Hotkey, &StreamDeckEvent, &[Hotkey]) -> anyhow::Result<()> + Send + Sync;
type UpdateCallback = dyn Fn() -> anyhow::Result<()> + Send + Sync;
