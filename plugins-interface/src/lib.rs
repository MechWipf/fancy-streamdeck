use abi_stable::{
    declare_root_module_statics,
    library::{LibraryError, RootModule},
    package_version_strings,
    sabi_types::VersionStrings,
    std_types::{RStr, RVec},
    StableAbi,
};

#[repr(C)]
#[derive(StableAbi)]
#[sabi(kind(Prefix(prefix_ref = FancyDeckPluginRef)))]
pub struct FancyDeckPlugin {
    pub run: extern "C" fn(args: RVec<RStr>),
}

impl RootModule for FancyDeckPluginRef {
    declare_root_module_statics! {FancyDeckPluginRef}
    const BASE_NAME: &'static str = "fancy_deck_plugin";
    const NAME: &'static str = "fancy_deck_base_plugins";
    const VERSION_STRINGS: VersionStrings = package_version_strings!();

    fn initialization(self) -> Result<Self, LibraryError> {
        Ok(self)
    }
}
